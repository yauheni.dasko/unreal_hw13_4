#include <iostream>
#include "Helpers.h"

int main()
{
    int x, y;
    std::cout << "Enter the x value: ";
    std::cin >> x;
    std::cout << "Enter the y value: ";
    std::cin >> y;
    std::cout << "The square of the sum x and y: " << summ_square(x, y);

    return 0;
}
